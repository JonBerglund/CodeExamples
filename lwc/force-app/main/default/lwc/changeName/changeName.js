import { LightningElement, api } from 'lwc';
import NameInputChild from '@salesforce/label/c.NameInputChild';

// import helloWorld from 'helloWorld';

export default class ChangeName extends LightningElement {
    label = {
        NameInputChild
    };
    @api name;
    changeHandler(event) {
        this.name = event.target.value;
        const newEvent = new CustomEvent('changehandler',{detail: this.name});
        this.dispatchEvent(newEvent);
    }
}