import { LightningElement, track } from 'lwc';
import Contact_Information from '@salesforce/label/c.Contact_Information';

export default class HelloIteration extends LightningElement {
    label = {
        Contact_Information
    };

    @track contacts = [
        {
            Id: 1,
            Name: 'Amy Taylor',
            Title: 'VP of Engineering',
        },
        {
            Id: 2,
            Name: 'Michael Jones',
            Title: 'VP of Sales',
        },
        {
            Id: 3,
            Name: 'Jennifer Wul',
            Title: 'CEO',
        },
    ];
}