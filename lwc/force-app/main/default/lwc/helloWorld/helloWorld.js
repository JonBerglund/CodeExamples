import { LightningElement, wire, track, api } from 'lwc';
import { getRecord } from 'lightning/uiRecordApi';
import Hello from '@salesforce/label/c.Hello';
import MyNameIs from '@salesforce/label/c.My_Name_Is';
import Greeting from '@salesforce/label/c.Greeting';
import NameInputParent from '@salesforce/label/c.NameInputParent';
// import userId from '@salesforce/user/Id';
import USER_ID from '@salesforce/user/Id';
import NAME_FIELD from '@salesforce/schema/User.Name';

export default class HelloWorld extends LightningElement {
    label = {
        Hello,
        MyNameIs,
        Greeting,
        NameInputParent
    };
    @track name;
    @wire(getRecord, {
        recordId: USER_ID,
        fields: [NAME_FIELD]
    }) wireuser({
        error,
        data
    }) {
        if (error) {
           this.error = error ; 
        } else if (data) {
            // this.email = data.fields.Email.value;
            this.name = data.fields.Name.value;
        }
    }
    @track greeting;
    @api newName;

    
    constructor() {
        super();
        this.changeName = this.changeName.bind(this); // bind function to "this"
        this.changeHandlerEvent = this.changeHandlerEvent.bind(this);
        // this.template.addEventListener('changeHandlerEvent', this.changeHandler.bind(this));// Create an event listener programatically.
    }

    changeHandler(event) {
        this.changeName(event.target.value);
    }

    changeHandlerEvent(event) {
        const newName = event.detail;
        this.changeName(newName);
    }

    changeName(newName){
            this.newName = newName;
    }
}