# SFDX  App
First time lwc.

Include:
  - Input fields.
  - Child inside parent.
  - Labels.
  - Track/api attributes.
  - Child-parent event.
  - Wire retrieve user name.
  - Metadata.
  - HTML iteration.
  - Imports

## Dev, Build and Test
To use, need to pull and use SFDX to connnect your salesforce enviorment.
After connection established deploy the code.
When deploy finished you can enter salesforce enviorment ( if alreadt logged in logout and login again ) go to any tab home page and select edit page option.
There you can find the two components in custom components - just drag one of them to your page.

## Resources
Managed to create components using https://rajvakati.com/ , https://developer.salesforce.com/docs/component-library/documentation/lwc/ , https://trailhead.salesforce.com/content/learn/projects/quick-start-lightning-web-components

## Description of Files and Directories
Files included inside force-app/main/default/lwc are default lwc files and 3 components.
helloWorld, changeName, myFirstWebComponent

hellowWorld imports changeName and using it in html - changeName can change helloWorld newName attribute due to event.
